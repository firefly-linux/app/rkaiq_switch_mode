#!/bin/bash

cmd="/usr/bin/rkaiq_switch_mode"
config="/etc/init.d/.ffswitchmode_config"
adc_night=$(cat $config | grep ADC_NIGHT | awk -F '=' '{print $2}')
adc_day=$(cat $config | grep ADC_DAY | awk -F '=' '{print $2}')
adc_dev=$(cat $config | grep ADC_DEV | awk -F '=' '{print $2}')
mode=""

while true
do
	cur=$(cat "$adc_dev") 
	if [ $cur -lt $adc_day ] && [ "$mode" != "day" ];then
		mode="day"
		$cmd "set" "off" > /dev/null
		v4l2-ctl -d /dev/v4l-subdev4  --set-ctrl 'band_stop_filter=1'
	elif [ $cur -gt $adc_night ] && [ "$mode" != "night" ];then
		mode="night"
		$cmd "set" "on" > /dev/null
		v4l2-ctl -d /dev/v4l-subdev4  --set-ctrl 'band_stop_filter=0'
	fi
	sleep 0.5
done
